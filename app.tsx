import "./App.css";
import { usePasswordless } from "amazon-cognito-passwordless-auth/react";

function App() {
  const {
    signOut,
    showAuthenticatorManager,
    toggleShowAuthenticatorManager,
    tokensParsed,
  } = usePasswordless();

  return (
    <div className="app">
      <h1>Passwordless Demo App</h1>
      <h2>Hello {tokensParsed?.idToken.email}!</h2>
      <button onClick={signOut}>Sign out</button>
      <button
        onClick={() => toggleShowAuthenticatorManager()}
        disabled={showAuthenticatorManager}
      >
        Manage authenticators
      </button>
    </div>
  );
}

export default App;

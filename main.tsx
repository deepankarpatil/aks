import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { Passwordless } from "amazon-cognito-passwordless-auth";
import {
  PasswordlessContextProvider,
  Fido2Toast,
  Passwordless as PasswordlessComponent,
} from "amazon-cognito-passwordless-auth/react";
import "amazon-cognito-passwordless-auth/passwordless.css";

Passwordless.configure({
  clientId: "<client_id>", //replace with client id of user pool
  cognitoIdpEndpoint: "<region>", //replace with region, e.g. "us-east-1"
  debug: console.trace,
  fido2: {
    baseUrl: "<FIDO2 base URL>", //replace with the FIDO2 base URL
    authenticatorSelection: {
      userVerification: "required",
    },
  },
});

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <PasswordlessContextProvider enableLocalUserCache={true}>
    <PasswordlessComponent
      brand={{
        backgroundImageUrl:
          "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Manhattan_in_the_distance_%28Unsplash%29.jpg/2880px-Manhattan_in_the_distance_%28Unsplash%29.jpg",
        customerName: "Amazon Web Services",
        customerLogoUrl:
          "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1280px-Amazon_Web_Services_Logo.svg.png",
      }}
    >
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </PasswordlessComponent>
    <Fido2Toast />
  </PasswordlessContextProvider>
);
